import { deleteMoney, noMoney, getRandom, addMoney } from '../files/functions.js'

if (sessionStorage.getItem('money')) {
	if (document.querySelector('.score')) {
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
} else {
	sessionStorage.setItem('money', 12000);
	if (document.querySelector('.score')) {
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
}

const window_width = document.documentElement.clientWidth;
const window_height = document.documentElement.clientHeight;

//========================================================================================================================================================
// Функция присвоения случайного класса анимациии money icon
const anim_items = document.querySelectorAll('.icon-anim img');
function getRandomAnimate() {
	let number = getRandom(0, 3);
	let arr = ['jump', 'scale', 'rotate'];
	let random_item = getRandom(0, anim_items.length);
	anim_items.forEach(el => {
		if (el.classList.contains('_anim-icon-jump')) {
			el.classList.remove('_anim-icon-jump');
		} else if (el.classList.contains('_anim-icon-scale')) {
			el.classList.remove('_anim-icon-scale');
		} else if (el.classList.contains('_anim-icon-rotate')) {
			el.classList.remove('_anim-icon-rotate');
		}
	})
	setTimeout(() => {
		anim_items[random_item].classList.add(`_anim-icon-${arr[number]}`);
	}, 100);
}

if (document.querySelector('.icon-anim img')) {
	setInterval(() => {
		getRandomAnimate();
	}, 20000);
}

const btnHome = document.querySelector('.controls-slot__btn-home');
function getRandomAnimate2() {
	let number = getRandom(1, 3);

	if (btnHome.classList.contains('_anim-1')) {
		btnHome.classList.remove('_anim-1');
	} else if (btnHome.classList.contains('_anim-2')) {
		btnHome.classList.remove('_anim-2');
	}

	setTimeout(() => {
		btnHome.classList.add(`_anim-${number}`);
	}, 100);
}

if (btnHome) {
	setInterval(() => {
		getRandomAnimate2();
	}, 20000);
}

//========================================================================================================================================================
const configSlot = {
	currentWin: 0,
	bet: 50,
	winCoeff: 50
}

//game-1
if (document.querySelector('.slot__body')) {
	document.querySelector('.slot__body').classList.add('_active');
	sessionStorage.setItem('current-bet', configSlot.bet);
	document.querySelector('.score').textContent = sessionStorage.getItem('money');
}

function writeCountWin(bet) {
	configSlot.currentWin += bet * configSlot.winCoeff;
}

class Slot {
	constructor(domElement, config = {}) {
		Symbol.preload();

		this.currentSymbols = [
			["1", "2", "3"],
			["4", "5", "6"],
			["7", "1", "2"],
			["3", "4", "5"],
			["6", "7", "1"],
		];

		this.nextSymbols = [
			["1", "2", "3"],
			["4", "5", "6"],
			["7", "1", "2"],
			["3", "4", "5"],
			["6", "7", "1"],
		];

		this.container = domElement;

		this.reels = Array.from(this.container.getElementsByClassName("reel")).map(
			(reelContainer, idx) =>
				new Reel(reelContainer, idx, this.currentSymbols[idx])
		);

		this.spinButton = document.querySelector('.controls-slot__button-spin');
		this.spinButton.addEventListener("click", () => {
			if ((+sessionStorage.getItem('money') >= +sessionStorage.getItem('current-bet'))) {
				this.spin();
			} else {
				noMoney('.check');
			}
		});

		if (config.inverted) {
			this.container.classList.add("inverted");
		}
		this.config = config;
	}

	spin() {
		deleteMoney(+sessionStorage.getItem('current-bet'), '.score', 'money');
		this.currentSymbols = this.nextSymbols;
		this.nextSymbols = [
			[Symbol.random(), Symbol.random(), Symbol.random()],
			[Symbol.random(), Symbol.random(), Symbol.random()],
			[Symbol.random(), Symbol.random(), Symbol.random()],
			[Symbol.random(), Symbol.random(), Symbol.random()],
			[Symbol.random(), Symbol.random(), Symbol.random()]
		];

		this.onSpinStart(this.nextSymbols);

		return Promise.all(
			this.reels.map((reel) => {
				reel.renderSymbols(this.nextSymbols[reel.idx]);
				return reel.spin();
			})
		).then(() => this.onSpinEnd(this.nextSymbols));
	}

	onSpinStart(symbols) {
		this.spinButton.classList.add('_hold');

		this.config.onSpinStart?.(symbols);
	}

	onSpinEnd(symbols) {
		this.spinButton.classList.remove('_hold');

		this.config.onSpinEnd?.(symbols);
	}
}

class Reel {
	constructor(reelContainer, idx, initialSymbols) {
		this.reelContainer = reelContainer;
		this.idx = idx;

		this.symbolContainer = document.createElement("div");
		this.symbolContainer.classList.add("icons");
		this.reelContainer.appendChild(this.symbolContainer);

		this.animation = this.symbolContainer.animate(
			[
				{ transform: "none", filter: "blur(0)" },
				{ filter: "blur(2px)", offset: 0.5 },
				{
					transform: `translateY(-${((Math.floor(this.factor) * 10) /
						(3 + Math.floor(this.factor) * 10)) *
						100
						}%)`,
					filter: "blur(0)",
				},
			],
			{
				duration: this.factor * 1000,
				easing: "ease-in-out",
			}
		);
		this.animation.cancel();

		initialSymbols.forEach((symbol) =>
			this.symbolContainer.appendChild(new Symbol(symbol).img)
		);
	}

	get factor() {
		return 1 + Math.pow(this.idx / 2, 2);
	}

	renderSymbols(nextSymbols) {
		const fragment = document.createDocumentFragment();

		for (let i = 3; i < 3 + Math.floor(this.factor) * 10; i++) {
			const icon = new Symbol(
				i >= 10 * Math.floor(this.factor) - 2
					? nextSymbols[i - Math.floor(this.factor) * 10]
					: undefined
			);
			fragment.appendChild(icon.img);
		}

		this.symbolContainer.appendChild(fragment);
	}

	spin() {
		const animationPromise = new Promise(
			(resolve) => (this.animation.onfinish = resolve)
		);
		const timeoutPromise = new Promise((resolve) =>
			setTimeout(resolve, this.factor * 1000)
		);

		this.animation.play();

		return Promise.race([animationPromise, timeoutPromise]).then(() => {
			if (this.animation.playState !== "finished") this.animation.finish();

			const max = this.symbolContainer.children.length - 3; // 3 - количество картинок в одной колонке после остановки

			for (let i = 0; i < max; i++) {
				this.symbolContainer.firstChild.remove();
			}
		});
	}
}

const cache = {};

class Symbol {
	constructor(name = Symbol.random()) {
		this.name = name;

		if (cache[name]) {
			this.img = cache[name].cloneNode();
		} else {

			this.img = new Image();
			this.img.src = `img/game-1/slot-${name}.png`;

			cache[name] = this.img;
		}
	}

	static preload() {
		Symbol.symbols.forEach((symbol) => new Symbol(symbol));
	}

	static get symbols() {
		return [
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
		];
	}

	static random() {
		return this.symbols[Math.floor(Math.random() * this.symbols.length)];
	}
}

const config = {
	inverted: false,
	onSpinStart: (symbols) => {
	},
	onSpinEnd: (symbols) => {
		if (symbols[0][0] == symbols[1][0] && symbols[1][0] == symbols[2][0] && symbols[2][0] == symbols[3][0] && symbols[3][0] == symbols[4][0] ||
			symbols[0][1] == symbols[1][1] && symbols[1][1] == symbols[2][1] && symbols[2][1] == symbols[3][1] && symbols[3][1] == symbols[4][1] ||
			symbols[0][2] == symbols[1][2] && symbols[1][2] == symbols[2][2] && symbols[2][2] == symbols[3][2] && symbols[3][2] == symbols[4][2]) {

			let currintWin = +sessionStorage.getItem('current-bet') * configSlot.winCoeff;

			document.querySelector('.win-box__count').textContent = `win: ${currintWin}`
			document.querySelector('.win-box__count').classList.add('_visible');

			setTimeout(() => {
				document.querySelector('.win-box__count').classList.remove('_visible');

			}, 2000);

			writeCountWin(sessionStorage.getItem('current-bet'));
			addMoney(currintWin, '.score', 1000, 2000);
		}
	},
};

if (document.querySelector('.wrapper_game-1')) {
	const slot = new Slot(document.getElementById("slot"), config);
}

//========================================================================================================================================================
//game-2
class Slot2 {
	constructor(domElement, config2 = {}) {
		Symbol2.preload();

		this.currentSymbols = [
			["4", "2", "6"],
			["1", "3", "4"],
			["2", "6", "5"],
			["1", "4", "5"],
			["6", "2", "4"],
		];

		this.nextSymbols = [
			["4", "2", "6"],
			["1", "3", "4"],
			["2", "6", "5"],
			["1", "4", "5"],
			["6", "2", "4"],
		];

		this.container = domElement;

		this.reels = Array.from(this.container.getElementsByClassName("reel2")).map(
			(reelContainer, idx) =>
				new Reel2(reelContainer, idx, this.currentSymbols[idx])
		);

		this.spinButton = document.querySelector('.controls-slot__button-spin');
		this.spinButton.addEventListener("click", () => {
			if ((+sessionStorage.getItem('money') >= +sessionStorage.getItem('current-bet'))) {
				this.spin();
			} else {
				noMoney('.check');
			}
		});

		if (config2.inverted) {
			this.container.classList.add("inverted");
		}

		this.config = config2;
	}

	spin() {
		deleteMoney(+sessionStorage.getItem('current-bet'), '.score', 'money');
		this.currentSymbols = this.nextSymbols;
		this.nextSymbols = [
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
		];

		this.onSpinStart(this.nextSymbols);

		return Promise.all(
			this.reels.map((reel) => {
				reel.renderSymbols(this.nextSymbols[reel.idx]);
				return reel.spin();
			})
		).then(() => this.onSpinEnd(this.nextSymbols));
	}

	onSpinStart(symbols) {
		this.spinButton.classList.add('_hold');

		this.config.onSpinStart?.(symbols);
	}

	onSpinEnd(symbols) {
		this.spinButton.classList.remove('_hold');

		this.config.onSpinEnd?.(symbols);
	}
}

class Reel2 {
	constructor(reelContainer, idx, initialSymbols) {
		this.reelContainer = reelContainer;
		this.idx = idx;

		this.symbolContainer = document.createElement("div");
		this.symbolContainer.classList.add("icons");
		this.reelContainer.appendChild(this.symbolContainer);

		this.animation = this.symbolContainer.animate(
			[
				{ transform: "none", filter: "blur(0)" },
				{ filter: "blur(2px)", offset: 0.5 },
				{
					transform: `translateY(-${((Math.floor(this.factor) * 10) /
						(3 + Math.floor(this.factor) * 10)) *
						100
						}%)`,
					filter: "blur(0)",
				},
			],
			{
				duration: this.factor * 1000,
				easing: "ease-in-out",
			}
		);
		this.animation.cancel();

		initialSymbols.forEach((symbol) =>
			this.symbolContainer.appendChild(new Symbol2(symbol).img)
		);
	}

	get factor() {
		return 1 + Math.pow(this.idx / 2, 2);
	}

	renderSymbols(nextSymbols) {
		const fragment = document.createDocumentFragment();

		for (let i = 3; i < 3 + Math.floor(this.factor) * 10; i++) {
			const icon = new Symbol2(
				i >= 10 * Math.floor(this.factor) - 2
					? nextSymbols[i - Math.floor(this.factor) * 10]
					: undefined
			);
			fragment.appendChild(icon.img);
		}

		this.symbolContainer.appendChild(fragment);
	}

	spin() {
		const animationPromise = new Promise(
			(resolve) => (this.animation.onfinish = resolve)
		);
		const timeoutPromise = new Promise((resolve) =>
			setTimeout(resolve, this.factor * 1000)
		);

		this.animation.play();

		return Promise.race([animationPromise, timeoutPromise]).then(() => {
			if (this.animation.playState !== "finished") this.animation.finish();

			const max = this.symbolContainer.children.length - 3; // 3 - количество картинок в одной колонке после остановки

			for (let i = 0; i < max; i++) {
				this.symbolContainer.firstChild.remove();
			}
		});
	}
}

const cache2 = {};

class Symbol2 {
	constructor(name = Symbol2.random()) {
		this.name = name;

		if (cache2[name]) {
			this.img = cache2[name].cloneNode();
		} else {

			this.img = new Image();
			this.img.src = `img/game-2/slot-${name}.png`;

			cache2[name] = this.img;
		}
	}

	static preload() {
		Symbol2.symbols.forEach((symbol) => new Symbol2(symbol));
	}

	static get symbols() {
		return [
			'1',
			'2',
			'3',
			'4',
			'5',
			'6'
		];
	}

	static random() {
		return this.symbols[Math.floor(Math.random() * this.symbols.length)];
	}
}

const config2 = {
	inverted: false,
	onSpinStart: (symbols) => {
	},
	onSpinEnd: (symbols) => {
		if (symbols[0][0] == symbols[1][0] && symbols[1][0] == symbols[2][0] && symbols[2][0] == symbols[3][0] && symbols[3][0] == symbols[4][0] ||
			symbols[0][1] == symbols[1][1] && symbols[1][1] == symbols[2][1] && symbols[2][1] == symbols[3][1] && symbols[3][1] == symbols[4][1] ||
			symbols[0][2] == symbols[1][2] && symbols[1][2] == symbols[2][2] && symbols[2][2] == symbols[3][2] && symbols[3][2] == symbols[4][2]) {
			let currintWin = +sessionStorage.getItem('current-bet') * configSlot.winCoeff;

			document.querySelector('.win-box__count').textContent = `win: ${currintWin}`
			document.querySelector('.win-box__count').classList.add('_visible');

			setTimeout(() => {
				document.querySelector('.win-box__count').classList.remove('_visible');

			}, 2000);

			writeCountWin(sessionStorage.getItem('current-bet'));
			addMoney(currintWin, '.score', 1000, 2000);
		}
	},
};

if (document.querySelector('.wrapper_game-2')) {
	const slot = new Slot2(document.getElementById("slot2"), config2);
}

